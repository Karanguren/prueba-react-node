const categoryCtrl = {};

const Category = require('../models/categoryM');

categoryCtrl.getCategorys = async (req, res) => {
        const category = await Category.find();
        res.json(category);
    
};

categoryCtrl.createCategory = async (req, res) => {
    const { name } = req.body;
    const newCategory = new Category({
        name,
    });
    await newCategory.save();
    res.json('Save Category');
};


categoryCtrl.getCategory = async (req, res) => {
    const note = await Category.findById(req.params.id);
    res.json(note);
}

categoryCtrl.deleteCategory = async (req, res) => {
    const { id } = req.params;
    await Category.findByIdAndDelete(id);
    res.json('Category Deleted');
}

categoryCtrl.updateCategory = async (req, res) => {
    const { name } = req.body;
    await Category.findByIdAndUpdate(req.params.id, {
        name
    });
    res.json('Update Category');
}

module.exports = categoryCtrl;