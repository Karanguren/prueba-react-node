const userCtrl = {};

const jwt = require('jsonwebtoken')

const bcrypt = require('bcryptjs')



const User = require('../models/usersM');

process.env.SECRET_KEY = 'secret'


userCtrl.getUsers = async (req, res) => {
        const users = await User.find();
        res.json(users);
};

userCtrl.createUser = async (req, res) => {
    const { email, password, name, birthdate } = req.body;
    const emailUser = await User.findOne({ email: email });
        
        if (emailUser) {
            throw 'email "' + email + '" is already taken';
        } else {
            // Saving a New User
            const newUser = new User({ email, password, name, birthdate  });
            newUser.password = await newUser.encryptPassword(password);
            await newUser.save();
            res.json('User created');
        }

};

userCtrl.getUser = async (req, res) => {
    const user = await User.findById(req.params.id);
    res.json(user);
}

userCtrl.login = async (req, res) => {
    const { email, password } = req.body;

    const user = await User.findOne({email})
    
    if (user && bcrypt.compareSync(password, user.password)) {
      res.json(user)
      const { password, ...userWithoutHash } = user.toObject();
      const token = jwt.sign({ sub: user.id }, 'hola');
      return {
        ...userWithoutHash,
        token
      };
    } else {
      // Passwords don't match
      res.status(400).json({ message: 'Username or password is incorrect' })
    } 
}


userCtrl.deleteUser = async (req, res) => {
    const { id } = req.params;
    await User.findByIdAndDelete(id);
    res.json('User deleted');
}

userCtrl.updateUser = async (req, res) => {
    const { email, password, name, birthdate  } = req.body;
    await User.findByIdAndUpdate(req.params.id, {
        email, password, name, birthdate
    });
    res.json('User Actualizado');
}



module.exports = userCtrl;