const gastosCtrl = {};

const Gasto = require('../models/gastosM');

gastosCtrl.getGastos = async (req, res) => {
    const gastos = await Gasto.find();
    res.json(gastos);
};

gastosCtrl.createGasto = async (req, res) => {
    const { title, description, category, currency, amount, date} = req.body;
    const newGasto = new Gasto({
        title,
        description,
        category,
        currency,
        amount,
        date,
    });
    await newGasto.save();
    res.json('Gastos Guardado');
};

gastosCtrl.getGasto = async (req, res) => {
    const gasto = await Gasto.findById(req.params.id);
    res.json(gasto);
}

gastosCtrl.deleteGasto = async (req, res) => {
    await Gasto.findByIdAndDelete(req.params.id)
    res.json('Gastos Eliminado');
}

gastosCtrl.updateGasto = async (req, res) => {
    const { title, description, category, currency, amount, date} = req.body;
    await Gasto.findByIdAndUpdate(req.params.id, {
        title,
        description,
        category,
        currency,
        amount,
        date,
    });
    res.json('Gastos Actualizado');
}

module.exports = gastosCtrl;