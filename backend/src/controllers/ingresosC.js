const ingresosCtrl = {};

const Ingreso = require('../models/ingresosM');

ingresosCtrl.getIngresos = async (req, res) => {
    const ingresos = await Ingreso.find();
    res.json(ingresos);
};

ingresosCtrl.createIngreso = async (req, res) => {
    const { title, description, category, currency, amount, date } = req.body;
    const newIngreso = new Ingreso({
        title,
        description,
        category,
        currency,
        amount,
        date,
    });
    await newIngreso.save();
    res.json('Ingreso Guardado');
};

ingresosCtrl.getIngreso = async (req, res) => {
    const ingreso = await Ingreso.findById(req.params.id);
    res.json(ingreso);
}

ingresosCtrl.deleteIngreso = async (req, res) => {
    await Ingreso.findByIdAndDelete(req.params.id)
    res.json('Ingreso Eliminado');
}

ingresosCtrl.updateIngreso = async (req, res) => {
    const { title, description, category, currency, amount, date} = req.body;
    await Ingreso.findByIdAndUpdate(req.params.id, {
        title,
        description,
        category,
        currency,
        amount,
        date,
    });
    res.json('Ingreso Actualizado');
}

module.exports = ingresosCtrl;