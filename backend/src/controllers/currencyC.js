const currencyCtrl = {};

const Currency = require('../models/currencyM');

currencyCtrl.getCurrencys = async (req, res) => {
        const currency = await Currency.find();
        res.json(currency);
    
};

currencyCtrl.createCurrency = async (req, res) => {
    const { name } = req.body;
    const newCurrency = new Currency({
        name,
    });
    await newCurrency.save();
    res.json('Save Currency');
};


currencyCtrl.getCurrency = async (req, res) => {
    const note = await Currency.findById(req.params.id);
    res.json(note);
}

currencyCtrl.deleteCurrency = async (req, res) => {
    const { id } = req.params;
    await Currency.findByIdAndDelete(id);
    res.json('Currency Deleted');
}

currencyCtrl.updateCurrency = async (req, res) => {
    const { name } = req.body;
    await Currency.findByIdAndUpdate(req.params.id, {
        name
    });
    res.json('Update Currency');
}

module.exports = currencyCtrl;