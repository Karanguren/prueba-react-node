const express = require('express');
const cors = require('cors');
// const passport = require("passport");



const app = express();


// settings
app.set('port', process.env.PORT || 4000);


app.use(cors());
app.use(express.json());
// app.use(passport.initialize());

// require("./config/passport")(passport);


// routes
app.use('/api/user', require('./routes/usersR'));
app.use('/api/ingreso', require('./routes/ingresosR'));
app.use('/api/gasto', require('./routes/gastosR'));
app.use('/api/category', require('./routes/categoryR'));
app.use('/api/currency', require('./routes/currencyR'));
// app.use('/api/ingresos', require('./routes/ingresosR'));


module.exports = app;
