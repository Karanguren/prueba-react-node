const { Router } = require('express');
const router = Router();

const { getCategorys, createCategory, getCategory, deleteCategory, updateCategory } = require('../controllers/categoryC');

router.route('/')
    .get(getCategorys)
    .post(createCategory);

router.route('/:id')
    .get(getCategory)
    .delete(deleteCategory)
    .put(updateCategory);

module.exports = router;