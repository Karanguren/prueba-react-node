const { Router } = require('express');
const router = Router();

const { getIngresos, createIngreso, getIngreso, deleteIngreso, updateIngreso } = require('../controllers/ingresosC');

router.route('/')
    .get(getIngresos)
    .post(createIngreso);

router.route('/:id')
    .get(getIngreso)
    .delete(deleteIngreso)
    .put(updateIngreso);

module.exports = router;