const { Router } = require('express');
const router = Router();

const { getUsers, 
        createUser,
        getUser,
        deleteUser,
        updateUser,
        login,
    } = require('../controllers/usersC');

router.route('/')
    .get(getUsers)
    .post(createUser);

router.route('/:id')
    .get(getUser)
    .delete(deleteUser)
    .put(updateUser);

router.route('/login')
    .post(login)


module.exports = router;
