const { Router } = require('express');
const router = Router();

const { getCurrencys, createCurrency, getCurrency, deleteCurrency, updateCurrency } = require('../controllers/currencyC');

router.route('/')
    .get(getCurrencys)
    .post(createCurrency);

router.route('/:id')
    .get(getCurrency)
    .delete(deleteCurrency)
    .put(updateCurrency);

module.exports = router;