const { Router } = require('express');
const router = Router();

const { getGastos, createGasto, getGasto, deleteGasto, updateGasto } = require('../controllers/gastosC');

router.route('/')
    .get(getGastos)
    .post(createGasto);

router.route('/:id')
    .get(getGasto)
    .delete(deleteGasto)
    .put(updateGasto);

module.exports = router;