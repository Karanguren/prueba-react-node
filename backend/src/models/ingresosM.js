const { Schema, model } = require('mongoose');



const ingresoSchema = new Schema(
    {
        // user_id: { type: ObjectID, required: true},
        title: { type: String, required: true},
        description: { type: String},
        category: { type: String, required: true},
        currency: { type: String, required: true},
        amount: { type: Number, required: true},
        date: { type: Date}
        
    }, {
        timestamps: true
    });

module.exports = model('Ingreso', ingresoSchema);