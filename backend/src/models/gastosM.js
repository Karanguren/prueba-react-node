const { Schema, model } = require('mongoose');

const gastoSchema = new Schema(
    {
        // _id: { ObjectID },
        title: { type: String, required: true},
        description: { type: String},
        category: { type: String, required: true},
        currency: { type: String, required: true},
        amount: { type: Number, required: true},
        date: { type: Date}
    }, {
        timestamps: true
    });

module.exports = model('Gasto', gastoSchema);