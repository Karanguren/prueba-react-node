const { Schema, model } = require('mongoose');

const currencySchema = new Schema(
    {
        name: {type: String, required: true},
    }, {
        timestamps: true
    });

module.exports = model('Currency', currencySchema);