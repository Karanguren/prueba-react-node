const { Schema, model } = require('mongoose');

const bcrypt = require("bcryptjs");

const rolesValidos = {
    values: ["ADMIN", "USER"],
    message: '{VALUE} no es un role válido'
}

const userSchema = new Schema(
    {
        email: { type: String, required: true, unique: true},
        password: { type: String, required: true },
        name: { type: String },
        birthdate: { type: Date },
        role: {
            type: String,
            default: 'USER',
            required: [true],
            enum: rolesValidos,
        },
    }, {
        timestamps: true
});

userSchema.methods.encryptPassword = async password => {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
  };

userSchema.methods.matchPassword = async function(password) {
    return await bcrypt.compare(password, this.password);
};

module.exports = model('User', userSchema);