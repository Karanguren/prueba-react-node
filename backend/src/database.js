const mongoose = require('mongoose');

const URI = process.env.MONGOOSE_URI
    ? process.env.MONGOOSE_URI
    : 'mongodb://localhost/database';

mongoose.connect(URI, {
    useNewUrlParser: true,
    useCreateIndex: true
})
.then(() => console.log('MongoDB Connected'))
.catch(err => console.log(err))

const connection = mongoose.connection;

connection.once('open', () => {
    console.log('Database is connected');
});
