import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';


import './login.css';
import { actionsUser } from '../Actions/actionsUser';
import { actionsAlert } from '../Actions/actionsAlert';




class Login extends React.Component {
  constructor(props) {
    super(props);
    this.props.logout();

    this.state = {
        email: '',
        password: '',
        submitted: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ submitted: true });
    const { email, password } = this.state;

    var emailL = document.getElementById('email');
    var filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (!filter.test(emailL.value)) {
       this.props.errorE('Please provide a valid email address');

    }
    else if (email && password) {
        this.props.login(email, password);
    }
  }

  render() {
      const { loggingIn } = this.props;
      const { email, password, submitted } = this.state;
      return (
        <Grid container justify="center" alignItems="center">
            <Grid item xs={3} sm={4}></Grid>
            <Grid item xs={6} sm={4}>
                <Paper elevation={6} className="paperL"> 
                    <Avatar className="avatarL">
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                      Login
                    </Typography>
                        <form name="form" className="form" onSubmit={this.handleSubmit}>
                    <TextField
                        id="email"
                        value={email}
                        onChange={this.handleChange}
                        margin="normal"
                        fullWidth
                        label="Email"
                        name="email"
                        autoComplete="email"
                        autoFocus
                        error={(submitted && !email ? true : '')}
                        helperText={submitted && !email && "Email is required"}
                    />
                    <TextField
                        id="password"
                        value={password}
                        onChange={this.handleChange}
                        margin="normal"
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        autoComplete="current-password"
                        error={(submitted && !password ? true : '')}
                        helperText={submitted && !password && "Password is required"}
                    />
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        className="submit"
                        onClick={this.errorE}
                        fullWidth
                    >
                      login
                    {loggingIn && <CircularProgress size={24}/>}
                    </Button>
                    <br/>
                      <Link to="/register">Register</Link>
                  </form>    
                </Paper>
            </Grid>
            <Grid item xs={3} sm={4}></Grid>
        </Grid>
      );
  }
}

function mapState(state) {
  const { loggingIn } = state.authentication;
  return { loggingIn };
}

const actionCreators = {
  login: actionsUser.login,
  logout: actionsUser.logout,
  errorE: actionsAlert.error
};

const connectedLogin = connect(mapState, actionCreators)(Login);
export { connectedLogin as Login};

  
