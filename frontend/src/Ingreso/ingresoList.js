import React, { Component } from 'react';
import { Link } from 'react-router-dom'


import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper'
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import Typography from '@material-ui/core/Typography';


import axios from 'axios'
import './ingreso.css';



export default class TablaIngesos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ingresos: [],
    };

  }

  async componentDidMount() {
    this.getIngreso();
  }

  getIngreso = async () => {
    const res = await axios.get('http://localhost:4000/api/ingreso');
    this.setState({ ingresos: res.data })
    console.log(this.state.ingresos)
  }

  deleteIngreso = async (ingresoId) => {
    await axios.delete('http://localhost:4000/api/ingreso/' + ingresoId);
    this.getIngreso();
  }

  // total {

  // }


  render() {

    return (
      <Grid container justify="center" alignItems="center" className="table">
        <Grid item xs={12} sm={12} className="typogra2">
          <Typography component="h1" variant="h5" >
            Ingresos
          </Typography>
        </Grid>
        <Grid item xs={12} sm={11}>
          <TableContainer component={Paper}>
            <Table aria-label="simple table" size="small">
              <TableHead className="colorHead">
                <TableRow>
                  <TableCell>Titulo</TableCell>
                  <TableCell>Descripcion</TableCell>
                  <TableCell>Categoria</TableCell>
                  <TableCell>Monto</TableCell>
                  <TableCell>Moneda</TableCell>
                  <TableCell>Fecha y Hora</TableCell>
                  <TableCell>Acciones</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.ingresos.map(ingreso => (
                  <TableRow key={ingreso._id}>
                    <TableCell component="th" scope="row">
                      {ingreso.title}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {ingreso.description}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {ingreso.category}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {ingreso.amount}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {ingreso.currency}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {ingreso.date}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      <IconButton aria-label="delete" onClick={() => this.deleteIngreso(ingreso._id)}>
                        <DeleteIcon />
                      </IconButton>
                      <Link to={"/editI/" + ingreso._id}>
                        <IconButton color="primary" aria-label="edit">
                          <EditIcon />
                        </IconButton>
                      </Link>
                    </TableCell>
                  </TableRow>
                ))}
                  <TableRow>
                    <TableCell colSpan={2}>Total</TableCell>
                    <TableCell align="right"></TableCell>
                  </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
          <Link to={"/ingreso"}>
            <IconButton aria-label="add" color="secondary">
              <AddIcon />
            </IconButton>
          </Link>
        </Grid>
      </Grid>
    );
  }
}