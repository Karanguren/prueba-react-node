import { constantsUser } from '../Constants/constantsUser';
import { serviceUser } from '../Service/serviceUser';
import { actionsAlert } from './actionsAlert';
import { history } from '../Components/history';


export const actionsUser = {
    login,
    logout,
    createUser,
    getUser,
    getUsers,
    deleteUser,
    updateUser,
    registerUser
};

function login(email, password) {
    return dispatch => {
        dispatch(request({ email }));

        serviceUser.login(email, password)
            .then(
                user => { 
                    dispatch(success(user));
                    history.push('/');
                },
                error => {
                    dispatch(failure(error));
                    dispatch(actionsAlert.error(error));
                }
            );
    };

    function request(user) { return { type: constantsUser.LOGIN_REQUEST, user } }
    function success(user) { return { type: constantsUser.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: constantsUser.LOGIN_FAILURE, error } }
}

function logout() {
    serviceUser.logout();
    return { type: constantsUser.LOGOUT };
}

function createUser(user) {
    return dispatch => {
        dispatch(request(user));

        serviceUser.createUser(user)
            .then(
                user => { 
                    dispatch(success());
                    history.push('/home');
                    dispatch(actionsAlert.success('create user successful'));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(actionsAlert.error(error));
                }
            );
    };

    function request(user) { return { type: constantsUser.CREATE_REQUEST, user } }
    function success(user) { return { type: constantsUser.CREATE_SUCCESS, user } }
    function failure(error) { return { type: constantsUser.CREATE_FAILURE, error } }
}

function registerUser(user) {
    return dispatch => {
        dispatch(request(user));

        serviceUser.createUser(user)
            .then(
                user => { 
                    dispatch(success());
                    history.push('/');
                    dispatch(actionsAlert.success('Registration successful'));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(actionsAlert.error(error));
                }
            );
    };

    function request(user) { return { type: constantsUser.REGISTER_REQUEST, user } }
    function success(user) { return { type: constantsUser.REGISTER_SUCCESS, user } }
    function failure(error) { return { type: constantsUser.REGISTER_FAILURE, error } }
}

function getUsers() {
    return dispatch => {
        dispatch(request());

        serviceUser.getUsers()
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request(users) { return { type: constantsUser.GETALL_REQUEST, users } }
    function success(users) { return { type: constantsUser.GETALL_SUCCESS, users } }
    function failure(error) { return { type: constantsUser.GETALL_FAILURE, error } }
}

function getUser(id) {
    return dispatch => {
        dispatch(request(id));

        serviceUser.getUser()
            .then(
                user => dispatch(success(id)),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(user) { return { type: constantsUser.GETUSER_REQUEST, user } }
    function success(user) { return { type: constantsUser.GETUSER_SUCCESS, user } }
    function failure(error) { return { type: constantsUser.GETUSER_FAILURE, error } }
}


function updateUser(id) {
    return dispatch => {
        dispatch(request(id));

        serviceUser.updateUser()
            .then(
                user => dispatch(success(id)),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(user) { return { type: constantsUser.UPDATEUSER_REQUEST, user } }
    function success(user) { return { type: constantsUser.UPDATEUSER_SUCCESS, user } }
    function failure(error) { return { type: constantsUser.UPDATEUSER_FAILURE, error } }
}

//prefixed function name with underscore because delete is a reserved word in javascript
function deleteUser(id) {
    return dispatch => {
        dispatch(request(id));

        serviceUser.deleteUser(id)
            .then(
                user => dispatch(success(id)),
                history.push('/home'),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(id) { return { type: constantsUser.DELETE_REQUEST, id } }
    function success(id) { return { type: constantsUser.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: constantsUser.DELETE_FAILURE, id, error } }
}