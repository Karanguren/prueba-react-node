import { constantsAlert } from '../Constants/constantsAlert';

export const actionsAlert = {
    success,
    error,
    clear
};

function success(message) {
    return { type: constantsAlert.SUCCESS, message };
}

function error(message) {
    return { type: constantsAlert.ERROR, message };
}

function clear() {
    return { type: constantsAlert.CLEAR };
}