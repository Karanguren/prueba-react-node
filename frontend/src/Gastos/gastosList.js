import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper'
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import Typography from '@material-ui/core/Typography';


import './gasto.css';

import axios from 'axios'


export default class TablaGastos extends Component {
  constructor(props) {
    super(props);

    this.state = {
      gastos: [],
    };

  }

  async componentDidMount() {
    this.getGasto();
  }

  getGasto = async () => {
    const res = await axios.get('http://localhost:4000/api/gasto');
    this.setState({ gastos: res.data })
    console.log(this.state.gastos)
  }

  deleteGasto = async (gastoId) => {
    await axios.delete('http://localhost:4000/api/gasto/' + gastoId);
    this.getGasto();
  }

  render() {

    return (
      <Grid container justify="center" alignItems="center" className="table" >
        <Grid item xs={12} sm={12} className="typogra2">
          <Typography component="h1" variant="h5" >
            Gastos
          </Typography>
        </Grid>
        <Grid item xs={12} sm={11} >
          <TableContainer component={Paper}>
            <Table aria-label="simple table" size="small">
              <TableHead className="colorHead">
                <TableRow>
                  <TableCell>Titulo</TableCell>
                  <TableCell>Descripcion</TableCell>
                  <TableCell>Categoria</TableCell>
                  <TableCell>Monto</TableCell>
                  <TableCell>Moneda</TableCell>
                  <TableCell>Fecha y Hora</TableCell>
                  <TableCell>Acciones</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.gastos.map(gasto => (
                  <TableRow key={gasto._id}>
                    <TableCell component="th" scope="row">
                      {gasto.title}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {gasto.description}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {gasto.category}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {gasto.amount}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {gasto.currency}
                    </TableCell>
                    <TableCell component="th" scope="row" format="MM/dd/yyyy">
                      {gasto.date}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      <IconButton aria-label="delete" onClick={() => this.deleteGasto(gasto._id)}>
                        <DeleteIcon />
                      </IconButton>
                      <Link to={"/editG/" + gasto._id}>
                        <IconButton color="primary" aria-label="edit">
                          <EditIcon />
                        </IconButton>
                      </Link>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <Link to={"/gasto"}>
            <IconButton aria-label="add" color="secondary">
              <AddIcon />
            </IconButton>
          </Link>
        </Grid>
      </Grid>
    );
  }
}