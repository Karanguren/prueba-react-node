import React, { Component } from 'react'


import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';

import './gasto.css';

import axios from 'axios'

export default class CreateGastos extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            description: '',
            category: '',
            categories: [],
            currency: '',
            currencies: [],
            users: [],
            amount: '',
            date: '',
            editing: false,
            _id: ''
        };

        this.handleChange = this.handleChange.bind(this);
    }



    async componentDidMount() {
        const res = await axios.get('http://localhost:4000/api/currency');
        const res1 = await axios.get('http://localhost:4000/api/category');

        console.log(res1)
        console.log(res)
        if (res.data.length > 0) {
            this.setState({
                currencies: res.data.map(currencies => currencies.name),
                currency: res.data[0].name
            })
        }
        if (res.data.length > 0) {
            this.setState({
                categories: res1.data.map(categories => categories.name),
                category: res1.data[0].name
            })
        }
        if (this.props.match.params.id) {
            const res = await axios.get('http://localhost:4000/api/gasto/' + this.props.match.params.id);
            this.setState({
                title: res.data.title,
                description: res.data.description,
                category: res.data.category,
                currency: res.data.currency,
                amount: res.data.amount,
                date: res.data.date,
                _id: res.data._id,
                editing: true
            });
        }

    }

    handleSubmit = async (e) => {
        e.preventDefault();
        if (this.state.editing) {
            const updatedGasto = {
                title: this.state.title,
                description: this.state.description,
                category: this.state.category,
                currency: this.state.currency,
                amount: this.state.amount,
                date: this.state.date,
            };
            await axios.put('http://localhost:4000/api/gasto/' + this.state._id, updatedGasto);
        } else {
            const newGasto = {
                title: this.state.title,
                description: this.state.description,
                category: this.state.category,
                currency: this.state.currency,
                amount: this.state.amount,
                date: this.state.date,
            };
            axios.post('http://localhost:4000/api/gasto', newGasto);
        }
        window.location.href = '/gastos';

    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <Grid container justify="center" alignItems="center">
                <Grid item xs={6} sm={8}>
                    <Paper elevation={6} className="paperG">
                        <Grid item xs={12} sm={12} className="typogra">
                            <Typography component="h1" variant="h5" >
                                Registro Gastos
                            </Typography>
                        </Grid>
                        <form name="form" className="form" onSubmit={this.handleSubmit}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id="title"
                                    value={this.state.title}
                                    onChange={this.handleChange}
                                    margin="normal"
                                    fullWidth
                                    name="title"
                                    label="Titulo"
                                    required
                                />
                            </Grid>
                            <Grid item xs={12} sm={12}>
                                <TextField
                                    id="descripcion"
                                    value={this.state.description}
                                    onChange={this.handleChange}
                                    margin="normal"
                                    fullWidth
                                    label="Descricion"
                                    name="description"
                                    autoComplete="description"
                                    autoFocus
                                />
                            </Grid>
                            <br />
                            <Grid item xs={6} sm={6}>
                                <InputLabel shrink id="category">
                                    Categoria
                                    </InputLabel>
                                <Select
                                    margin="normal"
                                    name="category"
                                    id="category"
                                    labelId="category"
                                    value={this.state.category}
                                    onChange={this.handleChange}
                                    displayEmpty
                                    fullWidth
                                    required
                                >
                                    {this.state.categories.map(categories => (
                                        <MenuItem key={categories} value={categories}>
                                            {categories}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </Grid>
                            <br />
                            <Grid item xs={6} sm={6}>
                                <InputLabel shrink id="currency">
                                    Moneda
                                    </InputLabel>
                                <Select
                                    margin="normal"
                                    labelId="dcurrency"
                                    id="currency"
                                    name="currency"
                                    value={this.state.currency}
                                    onChange={this.handleChange}
                                    displayEmpty
                                    fullWidth
                                    required
                                >
                                    {this.state.currencies.map(currencies => (
                                        <MenuItem key={currencies} value={currencies}>
                                            {currencies}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </Grid>
                            <Grid item xs={6} sm={6}>
                                <TextField
                                    id="amount"
                                    value={this.state.amount}
                                    onChange={this.handleChange}
                                    margin="normal"
                                    fullWidth
                                    name="amount"
                                    label="Monto"
                                    required
                                />
                            </Grid>
                            <Grid item xs={6} sm={6}>
                                <TextField
                                    id="date"
                                    value={this.state.date}
                                    onChange={this.handleChange}
                                    type="datetime-local"
                                    margin="normal"
                                    fullWidth
                                    name="date"
                                />
                            </Grid>
                            <br />
                            <Button
                                type="submit"
                                variant="contained"
                                color="primary"
                                className="submit"
                                fullWidth
                            >
                                Guardar
                            </Button>
                        </form>
                    </Paper>
                </Grid>
            </Grid>
        )
    }
}