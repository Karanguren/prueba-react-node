import React, { Component } from 'react';

import Grid from '@material-ui/core/Grid';

import IngresosTabla from '../Ingreso/ingresoList'
import GastosTabla from '../Gastos/gastosList'

import axios from 'axios'


export default class Tabla extends Component {
  constructor(props) {
    super(props);

    this.state = {
      gastos: [],
      ingresos: [],
    };

  }
  
  async componentDidMount() {
    this.getGasto();
    this.getIngreso();

  }

  getGasto = async () => {
    const res = await axios.get('http://localhost:4000/api/gasto');
    this.setState({ gastos: res.data })
    console.log(this.state.gastos)
  }

  getIngreso = async () => {
    const res = await axios.get('http://localhost:4000/api/ingreso');
    this.setState({ ingresos: res.data })
    console.log(this.state.ingresos)
  }


  render() {

    return (
      <Grid container justify="center" alignItems="center">
        <Grid item xs={12} sm={12}> Bienvenido</Grid>
        <Grid item xs={12} sm={12}>
            <IngresosTabla/>
            <GastosTabla/>
        </Grid>
        <Grid item xs={12} sm={2}>Balance:</Grid>
      </Grid>
    );
  }
}