import React from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';



import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper'
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';

import { actionsUser } from '../Actions/actionsUser';

import './users.css';



class Users extends React.Component {

  componentDidMount() {
    this.props.getUsers();
    console.log(this.props.getUsers())
    console.log(this.props)
  }

  handleDeleteUser(id) {
    this.props.deleteUser(id);
  }

  render() {
    const { user, users } = this.props;
    return (
      <Grid container justify="center" alignItems="center" className="table">
        <Grid item xs={12} sm={11}>
          <TableContainer component={Paper}>
            <Table aria-label="simple table" size="small">
              <TableHead className="colorHead">
                <TableRow>
                  <TableCell>Email</TableCell>
                  <TableCell>Nombre</TableCell>
                  <TableCell>Fecha Nacimiento</TableCell>
                  <TableCell>Acciones</TableCell>
                </TableRow>
              </TableHead>
              {users.items &&
                <TableBody>
                  {users.items.map((user) =>
                    <TableRow key={user._id} >
                      <TableCell component="th" scope="row">
                        {user.email}
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {user.name}
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {user.birthdate}
                      </TableCell>
                      <TableCell component="th" scope="row">
                        <IconButton aria-label="delete" onClick={() => this.handleDeleteUser(user._id)}>
                          <DeleteIcon />
                        </IconButton>
                        <Link to={"/editU/" + user._id}>
                          <IconButton aria-label="edit" color="primary">
                            <EditIcon />
                          </IconButton>
                        </Link>
                      </TableCell>
                    </TableRow>
                  )}

                </TableBody>
              }
            </Table>
          </TableContainer>
          <Link to={"/addU"}>
            <IconButton aria-label="add" color="secondary">
              <AddIcon />
            </IconButton>
          </Link>
        </Grid>
      </Grid>
    );
  }
}

function mapState(state) {
  const { users, authentication } = state;
  const { user } = authentication;
  return { user, users };
}

const actionCreators = {
  getUsers: actionsUser.getUsers,
  deleteUser: actionsUser.deleteUser
}

const connectedTabla = connect(mapState, actionCreators)(Users);
export { connectedTabla as Users };