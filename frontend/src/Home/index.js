import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';


import { actionsUser } from '../Actions/actionsUser';
import { Users} from '../Users/userList';
import './home.css';


class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
        user: {
          _id: '',
          name: '',
          birthdate: '',
          editing: false,
        },
    };

    // this.handleChange = this.handleChange.bind(this);
    // this.handleSubmit = this.handleSubmit.bind(this);
}
componentDidMount() {
  this.props.getUsers();
  console.log('HOLA ' + this.props.getUsers())
  console.log(this.props)
}

handleChange= (e) =>  {
  this.setState({
    [e.target.name]: e.target.value
})

}
handleUpdateUser(id) {
  return (e) => this.props.updateUser(id);
}

render() {
  const { user } = this.props;
  const bull = <span className="bullet">•</span>;

  return (
    <Grid container justify="center" alignItems="center">
      <Link to="/login">Logout</Link>
        
        <Grid item xs={6} sm={3}></Grid>
        <Grid item xs={6} sm={6}>
                <Paper elevation={6} className="paper"> 
                    <Typography variant="h2" component="h2" align="center" className="color">
                        {bull}BIENVENIDO{bull}
                    </Typography>
                    <Typography variant="h6" component="p" align="center">
                        Ha iniciado de manera exitosa
                    </Typography>
                    <Typography variant="h4" align="center" >
                        *{user.name}*
                    </Typography>
                </Paper>
        </Grid>
        <Grid item xs={6} sm={3}></Grid>
        <Grid item xs={4} sm={11}>
            <Users />
        </Grid>
    </Grid>
  );

}
}

function mapState(state) {
  const { authentication } = state;
  const { user } = authentication;
  return { user };
}

const actionCreators = {
  getUsers: actionsUser.getUsers,

}

const connectedHome = connect(mapState, actionCreators)(Home);
export { connectedHome as Home };