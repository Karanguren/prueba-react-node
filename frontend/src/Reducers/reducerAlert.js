import { constantsAlert } from '../Constants/constantsAlert';

export function alert(state = {}, action) {
  switch (action.type) {
    case constantsAlert.SUCCESS:
      return {
        type: 'alert-success',
        message: action.message
      };
    case constantsAlert.ERROR:
      return {
        type: 'alert-danger',
        message: action.message
      };
    case constantsAlert.CLEAR:
      return {};
    default:
      return state
  }
}