import { constantsUser } from '../Constants/constantsUser';

export function registration(state = {}, action) {
  switch (action.type) {
    case constantsUser.REGISTER_REQUEST:
      return { registering: true };
    case constantsUser.REGISTER_SUCCESS:
      return {};
    case constantsUser.REGISTER_FAILURE:
      return {};
    default:
      return state
  }
}